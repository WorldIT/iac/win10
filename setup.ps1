Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Installing Scoop Repositories
scoop install scoop git
scoop bucket add extras
scoop bucket add java
scoop bucket add ascent 'https://github.com/zwxi/ascent.git' 

# Installing Applications

$apps = @(
	"neovim"
)

